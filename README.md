# my-vue-app-doc

The following would be a normal procedure on when developing on the host...
But if you would like to run the docker version, scroll down.

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your unit tests
```
npm run test:unit
```

### Lints and fixes files
```
npm run lint
```

## The docker version

If you have make install you can run command like:


`make install` - install dependencies in the `nodemodules` a docker volume elsewhere on the file system.

`make test` - Runs unit tests

`make up` - Starts the dev server

`make down`  - Stops the dev server

`make build` - Builds the app

If you don't have make, just run the docker commands, eg to install run:

`docker-compose -f docker-compose.builder.yml run --rm install`


```make
install:
	docker-compose -f docker-compose.builder.yml run --rm install

build:
	docker-compose -f docker-compose.builder.yml run --rm build

test:
	docker-compose -f docker-compose.builder.yml run --rm test

up:
	docker-compose up

down:
	docker-compose down
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
