install:
	docker-compose -f docker-compose.builder.yml run --rm install

build:
	docker-compose -f docker-compose.builder.yml run --rm build

test:
	docker-compose -f docker-compose.builder.yml run --rm test

up:
	docker-compose up

down:
	docker-compose down
